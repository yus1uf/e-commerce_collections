const { client } = require("./connection.js");

async function crudOperation(data) {
    //creating connection with the database
    await client.connect()
    const db = client.db("Ecommerce");

    //Inserting data into database
    const insert = await db.collection("products").insertMany(data);
    console.log("Number of record inserted " + insert.insertedCount);

    //Reading data from database
    const read = await db.collection("products").find().toArray();
    console.log(read);

    //updating data into database
    let query = { name: "Striped Men Round Neck Yellow T-Shirt" }
    let updateQuery = { $set: { Base_Price: 999, Sell_Price: 450 } }
    const update = await db.collection("products").updateMany(query, updateQuery);
    console.log("Number of record updated " + update.modifiedCount)

    //Deleting data from database
    const deleted = await db.collection("products").deleteMany(query);
    console.log("Number of record deleted " + deleted.deletedCount)
}

var obj = [{
    name: "Apple iphone XR",
    thumbnail: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.RxPvjDJxkqcZknQqMPb0rAHaHa%26pid%3DApi&f=1",
    Product_Gallery: ["image1.jpg", "image2.jpg", "image3.jpg", "image4.jpg"],
    description: "The iPhone XR has arrived to scintillate our senses with a host of features, such as the Liquid Retina Display, a faster Face ID, and a powerful A12 Bionic Chip.",
    Base_Price: 47900,
    Sell_Price: 44999,
    Category_name: "Electronics",
    Tags: "Mobile",
    Additional_information: "64 GB ROM"
},
{
    name: "Striped Men Round Neck Yellow T-Shirt",
    thumbnail: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.RxPvjDJxkqcZknQqMPb0rAHaHa%26pid%3DApi&f=1",
    Product_Gallery: ["image1.jpg", "image2.jpg", "image3.jpg", "image4.jpg"],
    description: "Nice fitting and after one wash it's looking good no colour shade.",
    Base_Price: 1200,
    Sell_Price: 750,
    Category_name: "Fashion",
    Tags: "T-Shirt",
    Additional_information: "very soft & good quality"

},
{
    name: "Mi 4A PRO 80 cm (32 inch) HD Ready LED Smart Android TV",
    thumbnail: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.RxPvjDJxkqcZknQqMPb0rAHaHa%26pid%3DApi&f=1",
    Product_Gallery: ["image1.jpg", "image2.jpg", "image3.jpg", "image4.jpg"],
    description: "There is no fun in watching your favourite movie or show on a TV where the display quality is poor. Now, boost the fun and watch them all in good and clear-quality on this 80 cm (32) Mi smart TV.",
    Base_Price: 19999,
    Sell_Price: 14999,
    Category_name: "Home-Appliances",
    Tags: "Tv",
    Additional_information: "1 Year Warranty on Product and Additional 1 Year Warranty on Panel"

}];


crudOperation(obj)
    .then()
    .catch(console.error)
    .finally(() => client.close());