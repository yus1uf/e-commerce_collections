const { client } = require("./connection.js");

async function crudOperation(data) {
    //creating connection with the database
    await client.connect()
    const db = client.db("Ecommerce");

    //Inserting data into database
    const insert = await db.collection("tags").insertMany(data);
    console.log("Number of record inserted " + insert.insertedCount);

    //Reading data from database
    const read = await db.collection("tags").find().toArray();
    console.log(read);

    //updating data into database
    let query = { name: "Mobile" }
    let updateQuery = { $set: { "slug.1": "Samsung-Galaxy" } }
    const update = await db.collection("tags").updateMany(query, updateQuery);
    console.log("Number of record updated " + update.modifiedCount)

    //Deleting data from database
    const deleted = await db.collection("tags").deleteMany(query);
    console.log("Number of record deleted " + deleted.deletedCount)
}

var obj = [{
    name: "Jeans",
    "slug": ["slim-fit-jeans", "denim-jeans"]
},
{
    name: "T-Shirt",
    "slug": ["full-sleeve", "half-sleeve", "round-neck"]
},
{
    name: "Mobile",
    "slug": ["iphone-13-pro", "iphone-X", "redmi-note-10-pro"]
},
{
    name: "Laptop",
    "slug": ["HP-Probook", "Lenovo-Ideapad", "Macbook"]
},
{
    name: "TV",
    "slug": ["redmi-android-tv", "realme-smart-tv", "oneplus-smart-tv"]
}];
crudOperation(obj)
    .then()
    .catch(console.error)
    .finally(() => client.close());