const { client } = require("./connection.js");

async function crudOperation(data) {
    //creating connection with the database
    await client.connect()
    const db = client.db("Ecommerce");

    //Inserting data into database
    const insert = await db.collection("cart").insertMany(data);
    console.log("Number of record inserted " + insert.insertedCount);


    //Reading data from database
    const read = await db.collection("cart").find().toArray();
    console.log(read);

    //updating data into database
    let query = { product: "Apple iphone XR" }
    let updateQuery = { $set: { Product_qty: 2, Total_Price: 89998 } }
    const update = await db.collection("cart").updateMany(query, updateQuery);
    console.log("Number of record updated " + update.modifiedCount)

    //Deleting data from database
    const deleted = await db.collection("cart").deleteMany(query);
    console.log("Number of record deleted " + deleted.deletedCount)
}

var obj = [{
    product: "Apple iphone XR",
    user: "Aryan",
    Product_qty: 1,
    Base_Price: 47900,
    Sell_Price: 44999,
    Total_Price: 44999
},
{
    product: "Striped Men Round Neck Yellow T-Shirt",
    user: "Aryan",
    Product_qty: 3,
    Base_Price: 1200,
    Sell_Price: 750,
    Total_Price: 2250,
},
{
    product: "Mi 4A PRO 80 cm (32 inch) HD Ready LED Smart Android TV",
    user: "Dhiraj",
    Product_qty: 2,
    Base_Price: 19999,
    Sell_Price: 14999,
    Total_Price: 29998
}];


crudOperation(obj)
    .then()
    .catch(console.error)
    .finally(() => client.close());