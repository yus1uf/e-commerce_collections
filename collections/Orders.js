const { client } = require("./connection.js");

async function crudOperation(data) {
    //creating connection with the database
    await client.connect()
    const db = client.db("Ecommerce");

    //Inserting data into database
    const insert = await db.collection("orders").insertMany(data);
    console.log("Number of record inserted " + insert.insertedCount);

    //Reading data from database
    const read = await db.collection("orders").find().toArray();
    console.log(read);

    //updating data into database
    let query = { user_id: "Harsita123" }
    let updateQuery = { $set: { Transaction_Status: "Success", Payment_Status: "Success", Order_Status: "Placed" } }
    const update = await db.collection("orders").updateMany(query, updateQuery);
    console.log("Number of record updated " + update.modifiedCount)

    //Deleting data from database
    const deleted = await db.collection("orders").deleteMany(query);
    console.log("Number of record deleted " + deleted.deletedCount)
}

var obj = [{
    user_id: "Dhiraj9984",
    Total_items: 1,
    product: "Apple iphone XR",
    Base_Price: 47900,
    Sell_Price: 44999,
    Billing_Address: "19/6,Canttoment area, Varanasi",
    Shipping_Address: "19/6,Canttoment area, Varanasi",
    Transaction_Status: "Success",
    Payment_mode: "Cash On Delivery",
    Payment_Status: "Success",
    Order_Status: "Shipped"
},
{
    user_id: "Aryan5678",
    Total_items: 3,
    product: "Striped Men Round Neck Yellow T-Shirt",
    Base_Price: 1200,
    Sell_Price: 750,
    Billing_Address: "23/6, HSR Layout, Banglore Karnatka",
    Shipping_Address: "23/6, HSR Layout, Banglore Karnatka",
    Transaction_Status: "Failed",
    Payment_mode: "Debit Card",
    Payment_Status: "Failed",
    Order_Status: "Not Placed"
},
{
    user_id: "Harsita123",
    Total_items: 2,
    product: "Mi 4A PRO 80 cm (32 inch) HD Ready LED Smart Android TV",
    Base_Price: 19999,
    Sell_Price: 14999,
    Billing_Address: "A4, DLF Place, Saket District Centre, New Delhi",
    Shipping_Address: "A4, DLF Place, Saket District Centre, New Delhi",
    Transaction_Status: "Pending",
    Payment_mode: "EMI",
    Payment_Status: "Pending",
    Order_Status: "Not Placed"
}];


crudOperation(obj)
    .then()
    .catch(console.error)
    .finally(() => client.close());