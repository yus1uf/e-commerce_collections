const { client } = require("./connection.js");

async function crudOperation(data) {
    //creating connection with the database
    await client.connect()
    const db = client.db("Ecommerce");

    //Inserting data into database
    const insert = await db.collection("roles").insertMany(data);
    console.log("Number of record inserted " + insert.insertedCount);

    //Reading data from database
    const read = await db.collection("roles").find().toArray();
    console.log(read);

    //updating data into database
    let query = { name: "agent" }
    let updateQuery = { $set: { "slug.0": true } }
    const update = await db.collection("roles").updateMany(query, updateQuery);
    console.log("Number of record updated " + update.modifiedCount)

    //Deleting data from database
    const deleted = await db.collection("roles").deleteMany(query);
    console.log("Number of record deleted " + deleted.deletedCount)
}

var obj = [{
    name: "admin",
    "slug": [{ "create": true, "update": true, "delete": true, "upload_image": true }]
},
{
    name: "user",
    "slug": [{ "create": false, "update": false, "delete": false, "upload_image": true }]
},
{
    name: "agent",
    "slug": [{ "create": false, "update_product": true, "delete_product": true, "upload_image": true }]
}];
crudOperation(obj)
    .then()
    .catch(console.error)
    .finally(() => client.close());