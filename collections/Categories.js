const { client } = require("./connection.js");

async function crudOperation(data) {
    //creating connection with the database
    await client.connect()
    const db = client.db("Ecommerce");

    //Inserting data into database
    const insert = await db.collection("categories").insertMany(data);
    console.log("Number of record inserted " + insert.insertedCount);

    //Reading data from database
    const read = await db.collection("categories").find().toArray();
    console.log(read);

    //updating data into database
    let query = { name: "Home-Appliances" }
    let updateQuery = { $set: { image: "https://www.image.com/Air-Conditioner/" } }
    const update = await db.collection("categories").updateMany(query, updateQuery);
    console.log("Number of record updated " + update.modifiedCount)

    //Deleting data from database
    const deleted = await db.collection("categories").deleteMany(query);
    console.log("Number of record deleted " + deleted.deletedCount)
}

var obj = [{
    name: "Fashion",
    "slug": ["Men-Fashion", "Kids-Fashion", "Women-Fashion", "Jeans-for-Men"],
    image: "https://www.image.com/metronaut-color-block-women-round-neck-white-pink-t-shirt/",
    description: "Given how powerful social media has become these days, everyone around the world wants to look their best at all times. Thus, the right clothing and accessories are almost always in demand."
},
{
    name: "Home-Appliances",
    "slug": ["Air-Conditioner", "Washing-Machine", "Water-Purifiers", "Air-Purifiers"],
    image: "https://www.image.com/water-purifiers/",
    descriptin: "A home appliance, also referred to as a domestic appliance, an electric appliance or a household appliance, is a machine which assists in household functions such as cooking, cleaning and food preservation."
}];
crudOperation(obj)
    .then()
    .catch(console.error)
    .finally(() => client.close());