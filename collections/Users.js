const { client } = require("./connection.js");

async function crudOperation(data) {
  //creating connection with the database
  await client.connect()
  const db = client.db("Ecommerce");

  //Inserting data into database
  const insert = await db.collection("users").insertMany(data);
  console.log("Number of record inserted " + insert.insertedCount);

  //Reading data from database
  const read = await db.collection("users").find().toArray();
  console.log(read);

  //updating data into database
  let query = { first_name: "Aryan" }
  let updateQuery = { $set: { email: "aryan@gmail.com" } }
  const update = await db.collection("users").updateMany(query, updateQuery);
  console.log("Number of record updated " + update.modifiedCount)

  //Deleting data from database
  const deleted = await db.collection("users").deleteMany(query);
  console.log("Number of record deleted " + deleted.deletedCount)
}

var obj = [{
  id: 1,
  first_name: "Yusuf",
  last_name: "Shekh",
  email: "yusuf.s@valuebound.com",
  image: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.RxPvjDJxkqcZknQqMPb0rAHaHa%26pid%3DApi&f=1",
  role: "admin"
},
{
  id: 2,
  first_name: "Yogesh",
  last_name: "Pandey",
  email: "yogesh.p@accenture.com",
  image: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.RxPvjDJxkqcZknQqMPb0rAHaHa%26pid%3DApi&f=1",
  role: "agent"
},
{
  id: 3,
  first_name: "Garvit",
  last_name: "Kushwaha",
  email: "garvit.k@accenture.com",
  image: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.RxPvjDJxkqcZknQqMPb0rAHaHa%26pid%3DApi&f=1",
  role: "agent"
},
{
  id: 4,
  first_name: "Sohit",
  last_name: "Patel",
  email: "patel.sohit@cognizant.com",
  image: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.RxPvjDJxkqcZknQqMPb0rAHaHa%26pid%3DApi&f=1",
  role: "agent"
},
{
  id: 5,
  first_name: "Dhiraj",
  last_name: "Gupta",
  email: "guptadhiraj@infosys.com",
  image: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.RxPvjDJxkqcZknQqMPb0rAHaHa%26pid%3DApi&f=1",
  role: "user"
},
{
  id: 6,
  first_name: "Aryan",
  last_name: "Daftari",
  email: "daftariaryan@infosys.com",
  image: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.RxPvjDJxkqcZknQqMPb0rAHaHa%26pid%3DApi&f=1",
  role: "user"
},
{
  id: 7,
  first_name: "Arshjot",
  last_name: "Singh",
  email: "arshjot.singh@tcs.com",
  image: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.RxPvjDJxkqcZknQqMPb0rAHaHa%26pid%3DApi&f=1",
  role: "user"
},
{
  id: 8,
  first_name: "Harsita",
  last_name: "Singh",
  email: "harsita.singh@tcs.com",
  image: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.RxPvjDJxkqcZknQqMPb0rAHaHa%26pid%3DApi&f=1",
  role: "user"
},
{
  id: 9,
  first_name: "Pretty",
  last_name: "Malik",
  email: "pretty@infogain.com",
  image: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.RxPvjDJxkqcZknQqMPb0rAHaHa%26pid%3DApi&f=1",
  role: "user"
},
{
  id: 10,
  first_name: "Piyush",
  last_name: "Singh",
  email: "piyush@infogain.com",
  image: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.RxPvjDJxkqcZknQqMPb0rAHaHa%26pid%3DApi&f=1",
  role: "user"
},
];

crudOperation(obj)
  .then()
  .catch(console.error)
  .finally(() => client.close());